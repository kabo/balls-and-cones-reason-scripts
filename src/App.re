open Draggable;

type xy_coordinates = {
  x: float,
  y: float,
};

type two_xy_coordinates = {
  x1: float,
  y1: float,
  x2: float,
  y2: float,
};

let calcTangentCoordinates = (~cx: float, ~cy: float, ~r: float, ~px: float, ~py: float) => {
  /* http://www.ambrsoft.com/TrigoCalc/Circles2/CirclePoint/CirclePointDistance.htm */
  let rsq = r ** 2.0;
  let xpasq = (px -. cx) ** 2.0;
  let ypbsq = (py -. cy) ** 2.0;
  let x1 = ((rsq *. (px -. cx) +. r *. (py -. cy) *. sqrt(xpasq +. ypbsq -. rsq)) /. (xpasq +. ypbsq)) +. cx;
  let y1 = ((rsq *. (py -. cy) -. r *. (px -. cx) *. sqrt(xpasq +. ypbsq -. rsq)) /. (xpasq +. ypbsq)) +. cy;
  let x2 = ((rsq *. (px -. cx) -. r *. (py -. cy) *. sqrt(xpasq +. ypbsq -. rsq)) /. (xpasq +. ypbsq)) +. cx;
  let y2 = ((rsq *. (py -. cy) +. r *. (px -. cx) *. sqrt(xpasq +. ypbsq -. rsq)) /. (xpasq +. ypbsq)) +. cy;
  { x1, y1, x2, y2 };
};

let calcHomotheticCenter = (~x1: float, ~y1: float, ~r1: float, ~x2: float, ~y2: float, ~r2: float) => {
  /* https://en.wikipedia.org/wiki/Homothetic_center#Computing_homothetic_centers */
  let x = {
    let part1 = -. r2 *. x1 /. (r1 -. r2)
    part1 +. r1 *. x2 /. (r1 -. r2)
  };
  let y = {
    let part1 = -. r2 *. y1 /. (r1 -. r2)
    part1 +. r1 *. y2 /. (r1 -. r2)
  };
  { x, y };
};

let getInfiniteLine = (~x1: float, ~y1: float, ~x2: float, ~y2: float) => {
  /* https://www.mathsisfun.com/algebra/line-equation-2points.html */
  if (y1 === y2) {
    /* straight horizontal line */
    {
      x1: 0.0,
      y1: y1,
      x2: 800.0,
      y2: y1,
    };
  } else if (x1 === x2) {
    /* straight vertical line */
    {
      x1: x1,
      y1: 0.0,
      x2: x1,
      y2: 600.0,
    };
  } else {
    let m = (y2 -. y1) /. (x2 -. x1);
    let b = y1 -. m *. x1;
    {
      x1: 0.0,
      y1: b,
      x2: 800.0,
      y2: 800.0 *. m +. b,
    };
  }
};

type state = {
  xl: float,
  yl: float,
  rl: float,
  xm: float,
  ym: float,
  rm: float,
  xs: float,
  ys: float,
  rs: float,
};

type action = Drag(char, float, float);

let component = ReasonReact.reducerComponent("App");

let make = (_children) => {
  ...component,
  initialState: () => {
    xl: 400.0,
    yl: 400.0,
    rl: 100.0,
    xm: 500.0,
    ym: 200.0,
    rm: 50.0,
    xs: 300.0,
    ys: 200.0,
    rs: 20.0,
  },
  reducer: (action: action, state: state) => {
    switch (action) {
      | Drag(c, x, y) => {
        switch (c) {
          | 'l' => ReasonReact.Update({ ...state, xl: x, yl: y });
          | 'm' => ReasonReact.Update({ ...state, xm: x, ym: y });
          | 's' => ReasonReact.Update({ ...state, xs: x, ys: y });
        }
      }
    }
  },
  render: self => {
    let { xl, yl, rl, xs, ys, rs, xm, ym, rm } = self.state;
    let pLS = calcHomotheticCenter( ~x1=xl, ~y1=yl, ~r1=rl, ~x2=xs, ~y2=ys, ~r2=rs );
    let pLM = calcHomotheticCenter( ~x1=xl, ~y1=yl, ~r1=rl, ~x2=xm, ~y2=ym, ~r2=rm );
    let pMS = calcHomotheticCenter( ~x1=xm, ~y1=ym, ~r1=rm, ~x2=xs, ~y2=ys, ~r2=rs );
    let infLine = getInfiniteLine( ~x1=pLS.x, ~y1=pLS.y, ~x2=pMS.x, ~y2=pMS.y );
    let tangentsLS = calcTangentCoordinates( ~cx=xl, ~cy=yl, ~r=rl, ~px=pLS.x, ~py=pLS.y );
    let tangentsLM = calcTangentCoordinates( ~cx=xl, ~cy=yl, ~r=rl, ~px=pLM.x, ~py=pLM.y );
    let tangentsMS = calcTangentCoordinates( ~cx=xm, ~cy=ym, ~r=rm, ~px=pMS.x, ~py=pMS.y );
    <div className="App">
      <div className="play-area">
        <svg viewBox="0 0 800 600">
          <circle
            cx={string_of_float(pLS.x)}
            cy={string_of_float(pLS.y)}
            r={"5"}
          />
          <circle
            cx={string_of_float(pLM.x)}
            cy={string_of_float(pLM.y)}
            r={"5"}
          />
          <circle
            cx={string_of_float(pMS.x)}
            cy={string_of_float(pMS.y)}
            r={"5"}
          />
          <line
            stroke="red"
            x1={string_of_float(infLine.x1)}
            y1={string_of_float(infLine.y1)}
            x2={string_of_float(infLine.x2)}
            y2={string_of_float(infLine.y2)}
          />
          <line
            stroke="gray"
            x1={string_of_float(tangentsLS.x1)}
            y1={string_of_float(tangentsLS.y1)}
            x2={string_of_float(pLS.x)}
            y2={string_of_float(pLS.y)}
          />
          <line
            stroke="gray"
            x1={string_of_float(tangentsLS.x2)}
            y1={string_of_float(tangentsLS.y2)}
            x2={string_of_float(pLS.x)}
            y2={string_of_float(pLS.y)}
          />
          <line
            stroke="gray"
            x1={string_of_float(tangentsLM.x1)}
            y1={string_of_float(tangentsLM.y1)}
            x2={string_of_float(pLM.x)}
            y2={string_of_float(pLM.y)}
          />
          <line
            stroke="gray"
            x1={string_of_float(tangentsLM.x2)}
            y1={string_of_float(tangentsLM.y2)}
            x2={string_of_float(pLM.x)}
            y2={string_of_float(pLM.y)}
          />
          <line
            stroke="gray"
            x1={string_of_float(tangentsMS.x1)}
            y1={string_of_float(tangentsMS.y1)}
            x2={string_of_float(pMS.x)}
            y2={string_of_float(pMS.y)}
          />
          <line
            stroke="gray"
            x1={string_of_float(tangentsMS.x2)}
            y1={string_of_float(tangentsMS.y2)}
            x2={string_of_float(pMS.x)}
            y2={string_of_float(pMS.y)}
          />
          <Draggable
            onDrag={(_, data) => self.send(Drag('s', data |. x, data |. y))}
            defaultPosition={xy_coords(~x=xs, ~y=ys)}
          >
            <circle className="s" cx="0" cy="0" r={string_of_float(rs)} />
          </Draggable>
          <Draggable
            onDrag={(_, data) => self.send(Drag('m', data |. x, data |. y))}
            defaultPosition={xy_coords(~x=xm, ~y=ym)}
          >
            <circle className="m" cx="0" cy="0" r={string_of_float(rm)} />
          </Draggable>
          <Draggable
            onDrag={(_, data) => self.send(Drag('l', data |. x, data |. y))}
            defaultPosition={xy_coords(~x=xl, ~y=yl)}
          >
            <circle className="l" cx="0" cy="0" r={string_of_float(rl)} />
          </Draggable>
        </svg>
      </div>
      <p className="explanation">
        (ReasonReact.string("After seeing the "))
        <a href="https://hooktube.com/watch?v=lubGnk0UZt0" target="_blank" rel="noreferrer noopener">
          (ReasonReact.string("Numberphile episode on balls & cones"))
        </a>
        (ReasonReact.string(" I had to make this."))
        <br />
        <a href="https://gitlab.com/kabo/balls-and-cones-reason-scripts" target="_blank" rel="noreferrer noopener">
          (ReasonReact.string("Source"))
        </a>
      </p>
    </div>
  },
};


          /*
          */

