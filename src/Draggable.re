type error;

[@bs.deriving abstract]
type xy_coords = {
  x: float,
  y: float,
};

module Draggable = {

  [@bs.module "react-draggable"]
  external reactClass : ReasonReact.reactClass = "default";

  let make =
    (~onDrag: option((Js.t({..}), xy_coords) => unit) = ?,
     ~defaultPosition: option(xy_coords) = ?
    ) =>
    ReasonReact.wrapJsForReason
      (~reactClass,
       ~props=Js.Nullable.({
         "onDrag": fromOption(onDrag),
         "defaultPosition": fromOption(defaultPosition)
       })
      );

};
